import productAction from "./productActions"
import cartAction from "./cartAction";

const actions = {
    productAction,
    cartAction
}
export default actions;