import IProduct from "./IProduct";

export default interface IProductResult {
    totalItemCount: number,
    productList: IProduct[]
}
