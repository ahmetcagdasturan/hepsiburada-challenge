
export default interface IProduct {
    id: number,
    name: string,
    brand: string,
    color: string,
    price: number,
    createdDate: Date,
    isInCart: boolean
}